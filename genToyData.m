%genToyData.m______________________________________________________________________


%[EPOCHES, LABELS,COV]=genToyData(K,N,T,C,varargin)
%genToyData function generates artificial epochs. 
%input arguments:
%K: number of epochs (positive integer)
%N: number of channels (positive integer)
%T: number of samples in one epoch (positive integer)
%C: number of classes (different covariance matrices) in the training set (positive integer)
%optional input argument(s):
%'Covariances', COV: covariance matrices of each class. (cell array) If this
%  parameter is not defined, covariance matrices are selected randomly 
%
%Output arguments:
%EPOCHES: generated epoches (cell array with K cells)
%LABELS: class label of generated epoches (array with K elements)
%COV: covariance matrices (cell array with C cells)
%DATE:  02.10.2014
%AUTHOR:  Ayhan Yuksel
%Contact: yukselay@itu.edu.tr, ayhanyks@yahoo.com

% % Copyright (c) 2014, Yuksel A.
% % All rights reserved.
% % 
% % Redistribution and use in source and binary forms, with or without
% % modification, are permitted provided that the following conditions are
% % met:
% % 
% %     * Redistributions of source code must retain the above copyright
% %       notice, this list of conditions and the following disclaimer.
% %     * Redistributions in binary form must reproduce the above copyright
% %       notice, this list of conditions and the following disclaimer in
% %       the documentation and/or other materials provided with the distribution
% % 
% % THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% % AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% % IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
% % ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
% % LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
% % CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
% % SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
% % INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
% % CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
% % ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
% % POSSIBILITY OF SUCH DAMAGE.

function [EPOCHES, LABELS,COV]=genToyData(K,N,T,C,varargin)

nVarargs = length(varargin);
for k = 1:2:nVarargs
    str=varargin{k};
    if(strcmpi(str,'Covariances'))
        COV=varargin{k+1};        
    else
        fprintf('%s could not recognized\n',varargin{k});
    end
end

%generate epoch labels
clsid=1:C;
LABELS=randi(C,K,1);



%generate random covariance matrices (spd matrices) spesific to each class:

if(~exist('COV','var'))
    for c=1:C
        d=rand(N,1)+0.01;    % positive diagonal matrice , eigenvalues
        W=randn(N,N);        % eigen vectors
        COV{c}=W'*diag(d)*W; % covariance matrices
    end
end

%generate epochs (NxT)
mu=zeros(1,N);
for k=1:K
    c=LABELS(k);
    EPOCHES{k}=mvnrnd(mu, COV{c},T);
end
%
%
%
%
%
%
%