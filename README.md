This repository  contains basic functions for running Spatial Filter Network SFN algorithm.

link to SFN paper:

https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0125039

file includes the following '.m' files:

SFN_demo.m: 	a demo script calling toy data generation, SFN training and SFN classification functions.

genToyData.m:	genToyData function generates artificial epochs. 

SFN_train_LM:	creates a SFN network and train with the Levenberg-Marquardt algorithm

SFN_train_BP:	create a SFN network and train with the Back propagation algorithm

SFN_test:	Classify input data with SFN and generate outputs.

