%SFN_test.m___________________________________________________________

%function [OUT F]=SFN_test(SFN,INP)
%Classify input data with SFN and generate outputs.
%
%INPUTS:
%
%SFN: trained SFN network. (See SFN_train_LM or SFN_train_BP)
%INP: input epochs to be classified. (cell array with K elements)
%
%
%OUTPUTS:
%OUT: class labels of input data (array vector)
%F: output of first layer (log-variance features) to be classified with
%classifier layer. (KxM matrix)
%DATE:  02.10.2014
%AUTHOR:  Ayhan Yuksel 
%Contact: yukselay@itu.edu.tr, ayhanyks@yahoo.com

% % Copyright (c) 2014, Yuksel A.
% % All rights reserved.
% % 
% % Redistribution and use in source and binary forms, with or without
% % modification, are permitted provided that the following conditions are
% % met:
% % 
% %     * Redistributions of source code must retain the above copyright
% %       notice, this list of conditions and the following disclaimer.
% %     * Redistributions in binary form must reproduce the above copyright
% %       notice, this list of conditions and the following disclaimer in
% %       the documentation and/or other materials provided with the distribution
% % 
% % THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% % AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% % IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
% % ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
% % LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
% % CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
% % SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
% % INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
% % CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
% % ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
% % POSSIBILITY OF SUCH DAMAGE.

function [OUT F]=SFN_test(SFN,INP)

K=numel(INP);                           %number of epochs in test set
N=size(INP{1},2);                       %number of channels, number of input neurons
T=size(INP{1},1);                       %number of samples in an epoch



W=SFN.W;
V=SFN.V;
b=SFN.b;


N=size(W,1);            %W: NxM
M=size(W,2);            
O=size(V,2);            %V: MxO

%create desired output vector for each class
C=numel(SFN.CLS);


DSR=-ones(C,O);
for c=1:C
    DSR(c,c)=1;
end


  wl=sqrt(diag(W'*W));
  wlx=ones(N,1)*wl';
  W=W./wlx;

for k=1:K
        
    X=INP{k}';
    
      
    y=W'*X;
    
    f=log(var(y'))';
    F(k,:)=f';
    
    z=V'*f+b;
    Phi=tanh(z);
    
    
    
    for c=1:C
        d(c)=(DSR(c,:)-Phi')*(DSR(c,:)-Phi')';
    end
    [mind minx]=min(d);
    OUT(k)=SFN.CLS(minx);    

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%