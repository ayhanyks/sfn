%SFN_train_BP.m_________________________________________________________________________

%function SFNNET=SFN_train_LM(TREP,TRLB,varargin)
%create a SFN network and train with the Back propagation algorithm
%
%INPUTS:
%TREP: train set epochs (cell array)
%TRLB: train set labels (array)
%
%Optional arguments:
%'M', M:  number of neurons in hidden layer. (default: equals to number of
%classs)
%'EMIN', EMIN : error value for terminating the training (default: 1e-1)
%'ITRMAX', ITRMAX : maximum number of iterations for terminating the training (default: 1000)
%'mu',mu : learning rate of the algorithm. (default: 1e-3)
%'W',W : initial spatial filter layer matrix (defaullt: randomly generated)
%'V',V : initial classifier layer matrix (defaullt: randomly generated)
%'b',b : initial classifier layer bias vector (defaullt: randomly generated)
%'dbg',dbg :debug level (verbosity) (default: 0, no verbosity)
%
%
%OUTPUTS:
%SFNNET: structure holding the following elements:
%.W: spatial filter layer weight matrix
%.V: classifier layer weight matrix
%.b: classifier layer biases
%.lastErr: final error value of the network at the end of the training.
%.itr: final iteration value of the network at the end of the training.
%.mu:  final combination coefficient value at the end of the training.
%.CLS: set of class labels 
%DATE:  02.10.2014
%AUTHOR:  Ayhan Yuksel
%Contact: yukselay@itu.edu.tr, ayhanyks@yahoo.com

% % Copyright (c) 2014, Yuksel A.
% % All rights reserved.
% % 
% % Redistribution and use in source and binary forms, with or without
% % modification, are permitted provided that the following conditions are
% % met:
% % 
% %     * Redistributions of source code must retain the above copyright
% %       notice, this list of conditions and the following disclaimer.
% %     * Redistributions in binary form must reproduce the above copyright
% %       notice, this list of conditions and the following disclaimer in
% %       the documentation and/or other materials provided with the distribution
% % 
% % THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% % AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% % IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
% % ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
% % LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
% % CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
% % SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
% % INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
% % CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
% % ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
% % POSSIBILITY OF SUCH DAMAGE.

function SFNNET=SFN_train_BP(TREP,TRLB,varargin)


K=numel(TREP);                      %number of epochs in training set
N=size(TREP{1},2);                  %number of channels, number of input neurons
T=size(TREP{1},1);                  %number of samples in an epoch

CLSLB=unique(TRLB);           %class labels
C=numel(CLSLB);              %number of classes in the trainin set

%number of output neurons
if(C==2)
    O=1;
    %create desired output vectors
    for k=1:K
     cls=TRLB(k);   
     if(cls==CLSLB(1))
         DSR(k,1)=1;
     else
         DSR(k,1)=-1;
     end
    end
else
    O=C;
    %create desired output vectors
    DSR=-ones(K,O);
    for k=1:K
        cls=TRLB(k);
        cx=find(CLSLB==cls);
        DSR(k,cx)=1;
    end

end




%defult params
EMIN=1e-1;      %maximum acceptable error
ITRMAX=1000;    %maximum number of iteration
M=C;          %number of neurons in hidden layer.
mu=1e-3;
dbg=0;         %verbosity

%Network weights W V and b 
  


%read varargin

nVarargs = length(varargin);
for k = 1:2:nVarargs
    str=varargin{k};
    if(strcmpi(str,'M'))
        M=varargin{k+1};
    elseif(strcmpi(str,'EMAX'))
        EMIN=varargin{k+1};
    elseif(strcmpi(str,'ITRMAX'))
        ITRMAX=varargin{k+1};  
    elseif(strcmpi(str,'mu'))
        mu=varargin{k+1}; 

    %network weight may be given as a parameter
    elseif(strcmpi(str,'W'))
        W=varargin{k+1};
    elseif(strcmpi(str,'V'))
        V=varargin{k+1};    
    elseif(strcmpi(str,'b'))
        b=varargin{k+1};
 
        
    elseif(strcmpi(str,'dbg'))  
        dbg=varargin{k+1};
        
    else
        fprintf('%s could not recognized\n',varargin{k});
    end
end


if(~exist('W'))
    W=randn(N,M)/10;
end
if(~exist('V'))
    V=randn(M,O)/10;
end
if(~exist('b'))
    b=randn(O,1)/10;  
end


ME=EMIN;    %mean error
itr=0;      %iteration counter




E=zeros(K*O,1); %error of each output for each epoch


while(ME>=EMIN && itr<ITRMAX)
    itr=itr+1;
    
    
    wl=sqrt(diag(W'*W));
    wlx=ones(N,1)*wl';
    
    for k=1:K
        
        % forward net:
        X=TREP{k}';
        y=(W./wlx)'*X;
        v=var(y')';
        f=log(v);
        z=V'*f+b;
        Phi=tanh(z);
        
        % backward net:
        E(k)= 0.5*(DSR(k,:)'-Phi)'*(DSR(k,:)'-Phi);
        dephi=Phi-DSR(k,:)';
        dPhiZ=1-(Phi).^2;
        dzV=f;
        dzf=V;
        dfy=(1./v)*ones(1,T)*(2/T).*(y);
        dzb=ones(O,1);
        
        
        
        DV=((dephi.*dPhiZ)*dzV')';
        
        
        DB=dephi.*dPhiZ.*dzb;
        
        for m=1:M
            dyw=(X*wl(m)-W(:,m)*y(m,:))/(wl(m)^2);  
            DFYW(:,m)=dyw*dfy(m,:)';
        end
        
               
        DW=ones(N,1)*(V*(dephi.*dPhiZ))'.*DFYW;
        

        b=b-mu*DB;
        V=V-mu*DV;
        W=W-mu*DW;
 
        
        
    end
    
    ME=mean(E);
    errval(itr)=ME;
    
    if(dbg>0)
        disp(sprintf('ens=%d %0.4f', itr, ME));
    end
    
end

SFNNET.W=W;
SFNNET.V=V;
SFNNET.b=b;

SFNNET.lastErr=ME;
SFNNET.itr=itr;
SFNNET.mu=mu;
SFNNET.CLS=CLSLB;

%
%
%
%
%
%
%